package com.bilas;

import com.bilas.model.Kanban;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Kanban kanban = new Kanban();
        do {
//        System.out.println(Color.BLUE);
            System.out.println("1 - Add Task" + "   \n2 - In Progress");
            System.out.println("3 - Code Review" + " \n4 - in Test");
            System.out.println("5 - Done");
            System.out.println("L - Get Product List" + "\nQ - exit");
            System.out.println("Please, select menu point.");
//        System.out.println(Color.RESET);
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        System.out.println("add task name:");
                        String name = input.nextLine();
                        kanban.addTaskToList(name);
                        break;
                    case "2":
                        kanban.toDo();
                        break;
                    case "3":
                        kanban.inProgress();
                        break;
                    case "4":
                        kanban.codeReview();
                        break;
                    case "5":
                        kanban.inTest();
                        break;
                    case "L":
                        kanban.getTaskList();
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

