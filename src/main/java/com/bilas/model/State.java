package com.bilas.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {
    Logger LOGGER = LogManager.getLogger(State.class);

    default void toDo(Kanban kanban) {
        LOGGER.info("Task " + kanban + " can not be placed in 'TO DO' section");
    }

    default void inProgress(Kanban kanban) {
        LOGGER.info("Task " + kanban + " can not be placed in 'IN PROGRESS' section");
    }

    default void codeReview(Kanban kanban) {
        LOGGER.info("Task " + kanban + " can not be placed in 'CODE REVIEW' section");
    }

    default void inTest(Kanban kanban) {
        LOGGER.info("Task " + kanban + " can not be placed in 'TESTING' section");
    }

    default void done(Kanban kanban) {
        LOGGER.info("Task " + kanban + " can not be placed in 'DONE' section");
    }
}

