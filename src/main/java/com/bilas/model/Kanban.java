package com.bilas.model;

import com.bilas.model.stateimpl.ToDoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Kanban {
    private static final Logger LOGGER = LogManager.getLogger(Kanban.class);
    private State state;
    private List<Task> taskList = new ArrayList<>();

    public Kanban(){
        state = new ToDoImpl();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void toDo(){
        state.toDo(this);
    }

    public void inProgress(){
        if (taskList.isEmpty()){
            LOGGER.info("Task list is empty");
        } else {
            state.inProgress(this);
        }
    }

    public void codeReview(){
        state.codeReview(this);
    }

    public void inTest(){
        state.inTest(this);
    }

    public void done(){
        state.done(this);
    }

    public void addTaskToList(String name){
        Task task = new Task(name);
        taskList.add(task);
        LOGGER.info("Task " + task.getName() + "added to list");
    }

    public void getTaskList(){
        LOGGER.info(taskList);
        for (Task task : taskList) {
            System.out.println(task);
        }
    }
}
