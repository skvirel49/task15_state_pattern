package com.bilas.model.stateimpl;

import com.bilas.model.Kanban;
import com.bilas.model.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InTestImpl implements State {
    private static final Logger LOGGER = LogManager.getLogger(InProgressImpl.class);

    @Override
    public void inProgress(Kanban kanban) {
        kanban.setState(new InProgressImpl());
        LOGGER.info("task in progress");
    }

    @Override
    public void done(Kanban kanban) {
        kanban.setState(new DoneImpl());
        LOGGER.info("task done!");
    }
}
