package com.bilas.model.stateimpl;

import com.bilas.model.Kanban;
import com.bilas.model.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class InProgressImpl implements State {
    private static final Logger LOGGER = LogManager.getLogger(InProgressImpl.class);
    @Override
    public void toDo(Kanban kanban) {
        kanban.setState(new ToDoImpl());
        LOGGER.info("add more task.");
    }

    @Override
    public void codeReview(Kanban kanban) {
        kanban.setState(new CodeReviewImpl());
        LOGGER.info("task is Code Review");
    }
}
