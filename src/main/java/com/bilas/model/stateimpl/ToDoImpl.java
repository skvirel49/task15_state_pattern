package com.bilas.model.stateimpl;

import com.bilas.model.Kanban;
import com.bilas.model.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ToDoImpl implements State {
    private static final Logger LOGGER = LogManager.getLogger(ToDoImpl.class);
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void toDo(Kanban kanban) {
        LOGGER.info("add task name:");
        String name = SCANNER.nextLine();
        kanban.addTaskToList(name);
    }

    @Override
    public void inProgress(Kanban kanban) {
        kanban.setState(new InProgressImpl());
        LOGGER.info("task in progress");
    }


}
