package com.bilas.model.stateimpl;

import com.bilas.model.Kanban;
import com.bilas.model.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CodeReviewImpl implements State {
    private static final Logger LOGGER = LogManager.getLogger(CodeReviewImpl.class);

    @Override
    public void inTest(Kanban kanban) {
        kanban.setState(new InTestImpl());
        LOGGER.info("task in test");
    }

    @Override
    public void inProgress(Kanban kanban) {
        kanban.setState(new InProgressImpl());
        LOGGER.info("task in progress");
    }
}
