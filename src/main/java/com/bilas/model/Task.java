package com.bilas.model;

import com.bilas.model.stateimpl.ToDoImpl;

public class Task {
    private static int counter = 1;
    private String name;
    private int taskNumber;

    public Task(String name) {
        this.name = name.concat(String.valueOf(counter));
        taskNumber = counter;
        counter++;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Task{" +
//                "state=" + state +
                ", name='" + name + '\'' +
                ", taskNumber=" + taskNumber +
                '}';
    }
}
